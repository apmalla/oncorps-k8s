variable "name" {
  default = "kukudkoo.com"
}

variable "region" {
  default = "us-west-1"
}

variable "azs" {
  default = ["us-west-1a", "us-west-1b"]
  type    = "list"
}

variable "env" {
  default = "prod"
}

variable "vpc_cidr" {
  default = "10.10.0.0/16"
}
