output "cluster_name" {
  value = "prod.kukudkoo.com"
}

output "master_security_group_ids" {
  value = ["${aws_security_group.masters-prod-kukudkoo-com.id}"]
}

output "masters_role_arn" {
  value = "${aws_iam_role.masters-prod-kukudkoo-com.arn}"
}

output "masters_role_name" {
  value = "${aws_iam_role.masters-prod-kukudkoo-com.name}"
}

output "node_security_group_ids" {
  value = ["${aws_security_group.nodes-prod-kukudkoo-com.id}"]
}

output "node_subnet_ids" {
  value = ["subnet-1c235147", "subnet-d783a5b0"]
}

output "nodes_role_arn" {
  value = "${aws_iam_role.nodes-prod-kukudkoo-com.arn}"
}

output "nodes_role_name" {
  value = "${aws_iam_role.nodes-prod-kukudkoo-com.name}"
}

output "region" {
  value = "us-west-1"
}

output "subnet_ids" {
  value = ["subnet-19235142", "subnet-1c235147", "subnet-ca83a5ad", "subnet-d783a5b0"]
}

output "vpc_id" {
  value = "vpc-539a0034"
}

provider "aws" {
  region = "us-west-1"
}

resource "aws_autoscaling_attachment" "master-us-west-1a-1-masters-prod-kukudkoo-com" {
  elb                    = "${aws_elb.api-prod-kukudkoo-com.id}"
  autoscaling_group_name = "${aws_autoscaling_group.master-us-west-1a-1-masters-prod-kukudkoo-com.id}"
}

resource "aws_autoscaling_attachment" "master-us-west-1a-2-masters-prod-kukudkoo-com" {
  elb                    = "${aws_elb.api-prod-kukudkoo-com.id}"
  autoscaling_group_name = "${aws_autoscaling_group.master-us-west-1a-2-masters-prod-kukudkoo-com.id}"
}

resource "aws_autoscaling_attachment" "master-us-west-1b-1-masters-prod-kukudkoo-com" {
  elb                    = "${aws_elb.api-prod-kukudkoo-com.id}"
  autoscaling_group_name = "${aws_autoscaling_group.master-us-west-1b-1-masters-prod-kukudkoo-com.id}"
}

resource "aws_autoscaling_group" "master-us-west-1a-1-masters-prod-kukudkoo-com" {
  name                 = "master-us-west-1a-1.masters.prod.kukudkoo.com"
  launch_configuration = "${aws_launch_configuration.master-us-west-1a-1-masters-prod-kukudkoo-com.id}"
  max_size             = 1
  min_size             = 1
  vpc_zone_identifier  = ["subnet-1c235147"]

  tag = {
    key                 = "KubernetesCluster"
    value               = "prod.kukudkoo.com"
    propagate_at_launch = true
  }

  tag = {
    key                 = "Name"
    value               = "master-us-west-1a-1.masters.prod.kukudkoo.com"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/cluster-autoscaler/node-template/label/kops.k8s.io/instancegroup"
    value               = "master-us-west-1a-1"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/role/master"
    value               = "1"
    propagate_at_launch = true
  }

  metrics_granularity = "1Minute"
  enabled_metrics     = ["GroupDesiredCapacity", "GroupInServiceInstances", "GroupMaxSize", "GroupMinSize", "GroupPendingInstances", "GroupStandbyInstances", "GroupTerminatingInstances", "GroupTotalInstances"]
}

resource "aws_autoscaling_group" "master-us-west-1a-2-masters-prod-kukudkoo-com" {
  name                 = "master-us-west-1a-2.masters.prod.kukudkoo.com"
  launch_configuration = "${aws_launch_configuration.master-us-west-1a-2-masters-prod-kukudkoo-com.id}"
  max_size             = 1
  min_size             = 1
  vpc_zone_identifier  = ["subnet-1c235147"]

  tag = {
    key                 = "KubernetesCluster"
    value               = "prod.kukudkoo.com"
    propagate_at_launch = true
  }

  tag = {
    key                 = "Name"
    value               = "master-us-west-1a-2.masters.prod.kukudkoo.com"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/cluster-autoscaler/node-template/label/kops.k8s.io/instancegroup"
    value               = "master-us-west-1a-2"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/role/master"
    value               = "1"
    propagate_at_launch = true
  }

  metrics_granularity = "1Minute"
  enabled_metrics     = ["GroupDesiredCapacity", "GroupInServiceInstances", "GroupMaxSize", "GroupMinSize", "GroupPendingInstances", "GroupStandbyInstances", "GroupTerminatingInstances", "GroupTotalInstances"]
}

resource "aws_autoscaling_group" "master-us-west-1b-1-masters-prod-kukudkoo-com" {
  name                 = "master-us-west-1b-1.masters.prod.kukudkoo.com"
  launch_configuration = "${aws_launch_configuration.master-us-west-1b-1-masters-prod-kukudkoo-com.id}"
  max_size             = 1
  min_size             = 1
  vpc_zone_identifier  = ["subnet-d783a5b0"]

  tag = {
    key                 = "KubernetesCluster"
    value               = "prod.kukudkoo.com"
    propagate_at_launch = true
  }

  tag = {
    key                 = "Name"
    value               = "master-us-west-1b-1.masters.prod.kukudkoo.com"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/cluster-autoscaler/node-template/label/kops.k8s.io/instancegroup"
    value               = "master-us-west-1b-1"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/role/master"
    value               = "1"
    propagate_at_launch = true
  }

  metrics_granularity = "1Minute"
  enabled_metrics     = ["GroupDesiredCapacity", "GroupInServiceInstances", "GroupMaxSize", "GroupMinSize", "GroupPendingInstances", "GroupStandbyInstances", "GroupTerminatingInstances", "GroupTotalInstances"]
}

resource "aws_autoscaling_group" "nodes-prod-kukudkoo-com" {
  name                 = "nodes.prod.kukudkoo.com"
  launch_configuration = "${aws_launch_configuration.nodes-prod-kukudkoo-com.id}"
  max_size             = 3
  min_size             = 3
  vpc_zone_identifier  = ["subnet-1c235147", "subnet-d783a5b0"]

  tag = {
    key                 = "KubernetesCluster"
    value               = "prod.kukudkoo.com"
    propagate_at_launch = true
  }

  tag = {
    key                 = "Name"
    value               = "nodes.prod.kukudkoo.com"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/cluster-autoscaler/node-template/label/kops.k8s.io/instancegroup"
    value               = "nodes"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/role/node"
    value               = "1"
    propagate_at_launch = true
  }

  metrics_granularity = "1Minute"
  enabled_metrics     = ["GroupDesiredCapacity", "GroupInServiceInstances", "GroupMaxSize", "GroupMinSize", "GroupPendingInstances", "GroupStandbyInstances", "GroupTerminatingInstances", "GroupTotalInstances"]
}

resource "aws_ebs_volume" "a-1-etcd-events-prod-kukudkoo-com" {
  availability_zone = "us-west-1a"
  size              = 20
  type              = "gp2"
  encrypted         = false

  tags = {
    KubernetesCluster                         = "prod.kukudkoo.com"
    Name                                      = "a-1.etcd-events.prod.kukudkoo.com"
    "k8s.io/etcd/events"                      = "a-1/a-1,a-2,b-1"
    "k8s.io/role/master"                      = "1"
    "kubernetes.io/cluster/prod.kukudkoo.com" = "owned"
  }
}

resource "aws_ebs_volume" "a-1-etcd-main-prod-kukudkoo-com" {
  availability_zone = "us-west-1a"
  size              = 20
  type              = "gp2"
  encrypted         = false

  tags = {
    KubernetesCluster                         = "prod.kukudkoo.com"
    Name                                      = "a-1.etcd-main.prod.kukudkoo.com"
    "k8s.io/etcd/main"                        = "a-1/a-1,a-2,b-1"
    "k8s.io/role/master"                      = "1"
    "kubernetes.io/cluster/prod.kukudkoo.com" = "owned"
  }
}

resource "aws_ebs_volume" "a-2-etcd-events-prod-kukudkoo-com" {
  availability_zone = "us-west-1a"
  size              = 20
  type              = "gp2"
  encrypted         = false

  tags = {
    KubernetesCluster                         = "prod.kukudkoo.com"
    Name                                      = "a-2.etcd-events.prod.kukudkoo.com"
    "k8s.io/etcd/events"                      = "a-2/a-1,a-2,b-1"
    "k8s.io/role/master"                      = "1"
    "kubernetes.io/cluster/prod.kukudkoo.com" = "owned"
  }
}

resource "aws_ebs_volume" "a-2-etcd-main-prod-kukudkoo-com" {
  availability_zone = "us-west-1a"
  size              = 20
  type              = "gp2"
  encrypted         = false

  tags = {
    KubernetesCluster                         = "prod.kukudkoo.com"
    Name                                      = "a-2.etcd-main.prod.kukudkoo.com"
    "k8s.io/etcd/main"                        = "a-2/a-1,a-2,b-1"
    "k8s.io/role/master"                      = "1"
    "kubernetes.io/cluster/prod.kukudkoo.com" = "owned"
  }
}

resource "aws_ebs_volume" "b-1-etcd-events-prod-kukudkoo-com" {
  availability_zone = "us-west-1b"
  size              = 20
  type              = "gp2"
  encrypted         = false

  tags = {
    KubernetesCluster                         = "prod.kukudkoo.com"
    Name                                      = "b-1.etcd-events.prod.kukudkoo.com"
    "k8s.io/etcd/events"                      = "b-1/a-1,a-2,b-1"
    "k8s.io/role/master"                      = "1"
    "kubernetes.io/cluster/prod.kukudkoo.com" = "owned"
  }
}

resource "aws_ebs_volume" "b-1-etcd-main-prod-kukudkoo-com" {
  availability_zone = "us-west-1b"
  size              = 20
  type              = "gp2"
  encrypted         = false

  tags = {
    KubernetesCluster                         = "prod.kukudkoo.com"
    Name                                      = "b-1.etcd-main.prod.kukudkoo.com"
    "k8s.io/etcd/main"                        = "b-1/a-1,a-2,b-1"
    "k8s.io/role/master"                      = "1"
    "kubernetes.io/cluster/prod.kukudkoo.com" = "owned"
  }
}

resource "aws_elb" "api-prod-kukudkoo-com" {
  name = "api-prod-kukudkoo-com-85l2pm"

  listener = {
    instance_port     = 443
    instance_protocol = "TCP"
    lb_port           = 443
    lb_protocol       = "TCP"
  }

  security_groups = ["${aws_security_group.api-elb-prod-kukudkoo-com.id}"]
  subnets         = ["subnet-19235142", "subnet-ca83a5ad"]

  health_check = {
    target              = "SSL:443"
    healthy_threshold   = 2
    unhealthy_threshold = 2
    interval            = 10
    timeout             = 5
  }

  idle_timeout = 300

  tags = {
    KubernetesCluster = "prod.kukudkoo.com"
    Name              = "api.prod.kukudkoo.com"
  }
}

resource "aws_iam_instance_profile" "masters-prod-kukudkoo-com" {
  name = "masters.prod.kukudkoo.com"
  role = "${aws_iam_role.masters-prod-kukudkoo-com.name}"
}

resource "aws_iam_instance_profile" "nodes-prod-kukudkoo-com" {
  name = "nodes.prod.kukudkoo.com"
  role = "${aws_iam_role.nodes-prod-kukudkoo-com.name}"
}

resource "aws_iam_role" "masters-prod-kukudkoo-com" {
  name               = "masters.prod.kukudkoo.com"
  assume_role_policy = "${file("${path.module}/data/aws_iam_role_masters.prod.kukudkoo.com_policy")}"
}

resource "aws_iam_role" "nodes-prod-kukudkoo-com" {
  name               = "nodes.prod.kukudkoo.com"
  assume_role_policy = "${file("${path.module}/data/aws_iam_role_nodes.prod.kukudkoo.com_policy")}"
}

resource "aws_iam_role_policy" "masters-prod-kukudkoo-com" {
  name   = "masters.prod.kukudkoo.com"
  role   = "${aws_iam_role.masters-prod-kukudkoo-com.name}"
  policy = "${file("${path.module}/data/aws_iam_role_policy_masters.prod.kukudkoo.com_policy")}"
}

resource "aws_iam_role_policy" "nodes-prod-kukudkoo-com" {
  name   = "nodes.prod.kukudkoo.com"
  role   = "${aws_iam_role.nodes-prod-kukudkoo-com.name}"
  policy = "${file("${path.module}/data/aws_iam_role_policy_nodes.prod.kukudkoo.com_policy")}"
}

resource "aws_key_pair" "kubernetes-prod-kukudkoo-com-e4eb14a104cc55e6e251d1461ca74606" {
  key_name   = "kubernetes.prod.kukudkoo.com-e4:eb:14:a1:04:cc:55:e6:e2:51:d1:46:1c:a7:46:06"
  public_key = "${file("${path.module}/data/aws_key_pair_kubernetes.prod.kukudkoo.com-e4eb14a104cc55e6e251d1461ca74606_public_key")}"
}

resource "aws_launch_configuration" "master-us-west-1a-1-masters-prod-kukudkoo-com" {
  name_prefix                 = "master-us-west-1a-1.masters.prod.kukudkoo.com-"
  image_id                    = "ami-c6151ba6"
  instance_type               = "t2.medium"
  key_name                    = "${aws_key_pair.kubernetes-prod-kukudkoo-com-e4eb14a104cc55e6e251d1461ca74606.id}"
  iam_instance_profile        = "${aws_iam_instance_profile.masters-prod-kukudkoo-com.id}"
  security_groups             = ["${aws_security_group.masters-prod-kukudkoo-com.id}"]
  associate_public_ip_address = false
  user_data                   = "${file("${path.module}/data/aws_launch_configuration_master-us-west-1a-1.masters.prod.kukudkoo.com_user_data")}"

  root_block_device = {
    volume_type           = "gp2"
    volume_size           = 64
    delete_on_termination = true
  }

  lifecycle = {
    create_before_destroy = true
  }

  enable_monitoring = false
}

resource "aws_launch_configuration" "master-us-west-1a-2-masters-prod-kukudkoo-com" {
  name_prefix                 = "master-us-west-1a-2.masters.prod.kukudkoo.com-"
  image_id                    = "ami-c6151ba6"
  instance_type               = "t2.medium"
  key_name                    = "${aws_key_pair.kubernetes-prod-kukudkoo-com-e4eb14a104cc55e6e251d1461ca74606.id}"
  iam_instance_profile        = "${aws_iam_instance_profile.masters-prod-kukudkoo-com.id}"
  security_groups             = ["${aws_security_group.masters-prod-kukudkoo-com.id}"]
  associate_public_ip_address = false
  user_data                   = "${file("${path.module}/data/aws_launch_configuration_master-us-west-1a-2.masters.prod.kukudkoo.com_user_data")}"

  root_block_device = {
    volume_type           = "gp2"
    volume_size           = 64
    delete_on_termination = true
  }

  lifecycle = {
    create_before_destroy = true
  }

  enable_monitoring = false
}

resource "aws_launch_configuration" "master-us-west-1b-1-masters-prod-kukudkoo-com" {
  name_prefix                 = "master-us-west-1b-1.masters.prod.kukudkoo.com-"
  image_id                    = "ami-c6151ba6"
  instance_type               = "t2.medium"
  key_name                    = "${aws_key_pair.kubernetes-prod-kukudkoo-com-e4eb14a104cc55e6e251d1461ca74606.id}"
  iam_instance_profile        = "${aws_iam_instance_profile.masters-prod-kukudkoo-com.id}"
  security_groups             = ["${aws_security_group.masters-prod-kukudkoo-com.id}"]
  associate_public_ip_address = false
  user_data                   = "${file("${path.module}/data/aws_launch_configuration_master-us-west-1b-1.masters.prod.kukudkoo.com_user_data")}"

  root_block_device = {
    volume_type           = "gp2"
    volume_size           = 64
    delete_on_termination = true
  }

  lifecycle = {
    create_before_destroy = true
  }

  enable_monitoring = false
}

resource "aws_launch_configuration" "nodes-prod-kukudkoo-com" {
  name_prefix                 = "nodes.prod.kukudkoo.com-"
  image_id                    = "ami-c6151ba6"
  instance_type               = "t2.medium"
  key_name                    = "${aws_key_pair.kubernetes-prod-kukudkoo-com-e4eb14a104cc55e6e251d1461ca74606.id}"
  iam_instance_profile        = "${aws_iam_instance_profile.nodes-prod-kukudkoo-com.id}"
  security_groups             = ["${aws_security_group.nodes-prod-kukudkoo-com.id}"]
  associate_public_ip_address = false
  user_data                   = "${file("${path.module}/data/aws_launch_configuration_nodes.prod.kukudkoo.com_user_data")}"

  root_block_device = {
    volume_type           = "gp2"
    volume_size           = 128
    delete_on_termination = true
  }

  lifecycle = {
    create_before_destroy = true
  }

  enable_monitoring = false
}

resource "aws_route53_record" "api-prod-kukudkoo-com" {
  name = "api.prod.kukudkoo.com"
  type = "A"

  alias = {
    name                   = "${aws_elb.api-prod-kukudkoo-com.dns_name}"
    zone_id                = "${aws_elb.api-prod-kukudkoo-com.zone_id}"
    evaluate_target_health = false
  }

  zone_id = "/hostedzone/Z2TJKWA7UWIZ1D"
}

resource "aws_security_group" "api-elb-prod-kukudkoo-com" {
  name        = "api-elb.prod.kukudkoo.com"
  vpc_id      = "vpc-539a0034"
  description = "Security group for api ELB"

  tags = {
    KubernetesCluster                         = "prod.kukudkoo.com"
    Name                                      = "api-elb.prod.kukudkoo.com"
    "kubernetes.io/cluster/prod.kukudkoo.com" = "owned"
  }
}

resource "aws_security_group" "masters-prod-kukudkoo-com" {
  name        = "masters.prod.kukudkoo.com"
  vpc_id      = "vpc-539a0034"
  description = "Security group for masters"

  tags = {
    KubernetesCluster                         = "prod.kukudkoo.com"
    Name                                      = "masters.prod.kukudkoo.com"
    "kubernetes.io/cluster/prod.kukudkoo.com" = "owned"
  }
}

resource "aws_security_group" "nodes-prod-kukudkoo-com" {
  name        = "nodes.prod.kukudkoo.com"
  vpc_id      = "vpc-539a0034"
  description = "Security group for nodes"

  tags = {
    KubernetesCluster                         = "prod.kukudkoo.com"
    Name                                      = "nodes.prod.kukudkoo.com"
    "kubernetes.io/cluster/prod.kukudkoo.com" = "owned"
  }
}

resource "aws_security_group_rule" "all-master-to-master" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-prod-kukudkoo-com.id}"
  source_security_group_id = "${aws_security_group.masters-prod-kukudkoo-com.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "all-master-to-node" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.nodes-prod-kukudkoo-com.id}"
  source_security_group_id = "${aws_security_group.masters-prod-kukudkoo-com.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "all-node-to-node" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.nodes-prod-kukudkoo-com.id}"
  source_security_group_id = "${aws_security_group.nodes-prod-kukudkoo-com.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "api-elb-egress" {
  type              = "egress"
  security_group_id = "${aws_security_group.api-elb-prod-kukudkoo-com.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "https-api-elb-0-0-0-0--0" {
  type              = "ingress"
  security_group_id = "${aws_security_group.api-elb-prod-kukudkoo-com.id}"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "https-elb-to-master" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-prod-kukudkoo-com.id}"
  source_security_group_id = "${aws_security_group.api-elb-prod-kukudkoo-com.id}"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "master-egress" {
  type              = "egress"
  security_group_id = "${aws_security_group.masters-prod-kukudkoo-com.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "node-egress" {
  type              = "egress"
  security_group_id = "${aws_security_group.nodes-prod-kukudkoo-com.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "node-to-master-protocol-ipip" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-prod-kukudkoo-com.id}"
  source_security_group_id = "${aws_security_group.nodes-prod-kukudkoo-com.id}"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "4"
}

resource "aws_security_group_rule" "node-to-master-tcp-1-2379" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-prod-kukudkoo-com.id}"
  source_security_group_id = "${aws_security_group.nodes-prod-kukudkoo-com.id}"
  from_port                = 1
  to_port                  = 2379
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "node-to-master-tcp-2382-4001" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-prod-kukudkoo-com.id}"
  source_security_group_id = "${aws_security_group.nodes-prod-kukudkoo-com.id}"
  from_port                = 2382
  to_port                  = 4001
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "node-to-master-tcp-4003-65535" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-prod-kukudkoo-com.id}"
  source_security_group_id = "${aws_security_group.nodes-prod-kukudkoo-com.id}"
  from_port                = 4003
  to_port                  = 65535
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "node-to-master-udp-1-65535" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-prod-kukudkoo-com.id}"
  source_security_group_id = "${aws_security_group.nodes-prod-kukudkoo-com.id}"
  from_port                = 1
  to_port                  = 65535
  protocol                 = "udp"
}

resource "aws_security_group_rule" "ssh-external-to-master-0-0-0-0--0" {
  type              = "ingress"
  security_group_id = "${aws_security_group.masters-prod-kukudkoo-com.id}"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "ssh-external-to-node-0-0-0-0--0" {
  type              = "ingress"
  security_group_id = "${aws_security_group.nodes-prod-kukudkoo-com.id}"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

terraform = {
  required_version = ">= 0.9.3"
}
